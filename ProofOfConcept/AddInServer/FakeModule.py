from Server import Server

class FakeModule:
	def getCXXTypeForPythonType(self, pythonTypeName):
		return self.cxxName + "::" + pythonTypeName

	def __init__(self, name, cxxName):
		self.name = name
		self.cxxName = cxxName

	def callProxyMethod(self, methodName, *args):
		return Server.instance.currentAddinInstance.wrap(Server.instance.currentAddinInstance.callProxyMethod(self.name, methodName, *map(lambda a: Server.instance.currentAddinInstance.unwrap(a), args)))

	def __getattr__(self, name):
		if name.endswith('_swigregister'):
			cxxType = self.getCXXTypeForPythonType(name[:-len('_swigregister')])
			return lambda *args: Server.instance.registerPythonTypeForCxxType(cxxType, *args)
		else:
			return lambda *args: self.callProxyMethod(name, *args)
