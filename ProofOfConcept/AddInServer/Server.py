import sys
import os


sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), 'Shared')))
sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'fusion_addins', 'example', 'protected')))


# sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'Shared')))

from AddinInstance import AddinInstance

class Server:
	def __init__(self):
		Server.instance = self
		self.cxxTypeToPythonType = dict()
	
	def run(self):
		import socket
		import sys
		import RealAddIn
		global messageQueue

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.bind(('localhost', 1235))
		s.listen(0)

		while True:
			print('Waiting for connection...')
			conn, addr = s.accept()
			print(f'Connected by {addr}')

			self.currentAddinInstance = AddinInstance(RealAddIn, conn)
			self.currentAddinInstance.runUntilStopped()

	def registerPythonTypeForCxxType(self, cxxType, pythonType):
		self.cxxTypeToPythonType[cxxType] = pythonType
