# Title: RealAddin.py
# Description: A sample Fusion 360 Add-In that talks to the CADPRO Desktop App
# Author: James Kingsthwaite
# Last Modified: 10/09/2019

import adsk.core, adsk.fusion, adsk.cam, traceback
import os
from datetime import datetime
from random import randint

# Globals
handlers = []
product_code = "CPSP-MZKIJS"

def getProductCode():
    # Returns the product code so that the server can check if a license is available
    global product_code
    return product_code

class MyMouseClickHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        app = adsk.core.Application.get()
        ui = app.userInterface
        try:
            # Draws a circle onto the design frame
            design = app.activeProduct

            # Get the root component of the active design.
            rootComp = design.rootComponent

            # Create a new sketch on the xy plane.
            sketches = rootComp.sketches
            xyPlane = rootComp.xYConstructionPlane
            sketch = sketches.add(xyPlane)

            # Draw some circles.
            circles = sketch.sketchCurves.sketchCircles
            circle1 = circles.addByCenterRadius(adsk.core.Point3D.create(0, 0, 0), randint(1, 5))
            circle2 = circles.addByCenterRadius(adsk.core.Point3D.create(randint(1, 10),randint(1, 10), randint(1, 10)), randint(1, 5))

            # Add a circle at the center of one of the existing circles.
            circle3 = circles.addByCenterRadius(circle2.centerSketchPoint, randint(1, 5))
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

class CommandCreatedEventHandler(adsk.core.CommandCreatedEventHandler):
    def __init__(self):
        super().__init__() 
    def notify(self, args):
        app = adsk.core.Application.get()
        ui = app.userInterface
        try:
            cmd = args.command                  
            # onClick = MyMouseClickHandler()
            # cmd.mouseClick.add(onClick)
            # handlers.append(onClick)

            onExecute = MyMouseClickHandler()
            cmd.execute.add(onExecute)
            handlers.append(onExecute)
        except:
            if ui:
                ui.messageBox(('Panel command created failed: {}').format(traceback.format_exc()))

def createUI(ui): 
    try:
        # For this example, we are adding the already exisiting 'Extrude' command into a new panel:
        cmdDefinitions = ui.commandDefinitions
        
        # For a few months, the customer might run either classic UI or tabbed toolbar UI.
        # Find out what is being used:
        print()
        print(ui)
        print()
        runningTabbedToolbar = True

        # UI manipulation testing
        workspace = ui.activeWorkspace
        design_toolbar_tabs = workspace.toolbarTabs
        design_toolbar_tools_tab = design_toolbar_tabs.itemById('ToolsTab')
        design_toolbar_tools_tab_panels = design_toolbar_tools_tab.toolbarPanels
        brandNewPanel = design_toolbar_tools_tab_panels.add('jamesSamplePanelId', 'CADPRO Sample Addin', 'ToolsSelectPanel', False)
        if brandNewPanel:
            # We want this panel to be visible:
            brandNewPanel.isVisible = True
            # Access the controls that belong to the panel:
            newPanelControls = brandNewPanel.controls

            # ----------------- Custom ------------------
            button_example = cmdDefinitions.addButtonDefinition('jamesTestButtonId', 'Test Button 1', "Creates circles of random sizes in the current document.", './/resources/Test')

            button_control = newPanelControls.addCommand(button_example)
            button_control.isPromotedByDefault = True
            button_control.isPromoted = True

            onCreated = CommandCreatedEventHandler()
            button_example.commandCreated.add(onCreated)
            handlers.append(onCreated)
            # -------------------------------------------   
        else:
            ui.messageBox('Not using tabbed toolbar')


        # if runningTabbedToolbar:
        #     # 'DesignProductType' is the 'Design' workspace switcher.
        #     # Get all of the tabs for Design:
        #     allDesignTabs = ui.toolbarTabsByProductType('DesignProductType')
        #     if (allDesignTabs.count > 0):
        #         # Access the 'Tools' tab within Design:
        #         toolsTab = allDesignTabs.itemById('ToolsTab')
        #         if toolsTab:
        #             # Get all of the toolbar panels for the Tools tab:
        #             allToolsTabPanels = toolsTab.toolbarPanels

        #             # Has the panel been added already?
        #             # You'll get an error if you try to add this more than once to the tab.
        #             brandNewPanel = None
        #             brandNewPanel = allToolsTabPanels.itemById('jamesSamplePanelId')
        #             if brandNewPanel is None:
        #                 # We have not added the panel already.  Go ahead and add it.
        #                 # Put it before the Inspect panel.
        #                 brandNewPanel = allToolsTabPanels.add('jamesSamplePanelId', 'CADPRO Sample Addin', 'ToolsSelectPanel', False)

        #             if brandNewPanel:
        #                 # We want this panel to be visible:
        #                 brandNewPanel.isVisible = True
        #                 # Access the controls that belong to the panel:
        #                 newPanelControls = brandNewPanel.controls

        #                 # ----------------- Custom ------------------
        #                 button_example = cmdDefinitions.addButtonDefinition('jamesTestButtonId', 'Test Button 1', "Hello, I'm a button.", './/resources/Test')

        #                 button_control = newPanelControls.addCommand(button_example)
        #                 button_control.isPromotedByDefault = True
        #                 button_control.isPromoted = True

        #                 onCreated = CommandCreatedEventHandler()
        #                 button_example.commandCreated.add(onCreated)
        #                 handlers.append(onCreated)
        #                 # -------------------------------------------                
        # else:
        #     ui.messageBox('Not using tabbed toolbar')
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

def deleteUI(ui):
    try:
        # Get the UserInterface object and the CommandDefinitions collection.
        cmdDefs = ui.commandDefinitions

        # Delete the button definition.
        buttonExample = ui.commandDefinitions.itemById('jamesTestButtonId')
        if buttonExample:
            buttonExample.deleteMe()
            
        # Get panel the control is in.
        addInsPanel = ui.allToolbarPanels.itemById('jamesSamplePanelId')

        # Get and delete the button control.
        buttonControl = addInsPanel.controls.itemById('jamesTestButtonId')
        if buttonControl:
            buttonControl.deleteMe()
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

def run(license_handler):
    try:
        # Imports the Fusion App and UI
        app = adsk.core.Application.get()
        ui = app.userInterface

        # Performs the license check
        license_available = True
        if license_available:
            createUI(ui) 
            adsk.autoTerminate(False)
        else:
            ui.messageBox("Sorry, you don't have a license for this Add-In")
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

def stop():
    try:
        # Imports the Fusion App and UI
        app = adsk.core.Application.get()
        ui = app.userInterface

        deleteUI(ui)
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))