# Imports dependencies
import shutil
import os
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext


def create_file_DLL(file_name, path_to_file):
    # Creates a DLL for the given file
    ext_modules = [
        Extension(file_name,  [path_to_file]),
    ]

    setup(
        name = 'My Program',
        cmdclass = {'build_ext': build_ext},
        ext_modules = ext_modules
    )

    os.rename(file_name + '.cp37-win32.pyd', file_name + '.pyd')


if __name__ == "__main__":
    # To run, call 'python setup.py build_ext --inplace' in a terminal
    # Note: Comment out the calls that don't need to be compiled

    # Creates Server DLLs
    # create_file_DLL('Server', 'Server.py')
    # os.remove('Server.c')

    # create_file_DLL('LicenseHandler', 'LicenseHandler.py')
    # os.remove('LicenseHandler.c')

    # create_file_DLL('FakeModule', 'FakeModule.py')
    # os.remove('FakeModule.c')

    # # # Creates Shared DLLs
    # create_file_DLL('Message', './Shared/Message.py')
    # os.remove('./Shared/Message.c')
    # shutil.move("Message.pyd", './Shared')

    # create_file_DLL('MessageQueue', './Shared/MessageQueue.py')
    # os.remove('./Shared/MessageQueue.c')
    # shutil.move("MessageQueue.pyd", './Shared')

    # create_file_DLL('ObjectTable', './Shared/ObjectTable.py')
    # os.remove('./Shared/ObjectTable.c')
    # shutil.move("ObjectTable.pyd", './Shared')

    # # # Creates Real Addin DLL
    # create_file_DLL('RealAddin', './../fusion_addins/example/protected/RealAddin.py')
    # os.remove('../fusion_addins/example/protected/RealAddin.c')
    # shutil.move("RealAddin.pyd", './../fusion_addins/example/protected')

    # # Creates Dummy Addin DLL
    create_file_DLL('CADPRO-Sample-Fusion-Addin', './../fusion_addins/example/unprotected/CADPRO-Sample-Fusion-Addin.py')
    os.remove('./../fusion_addins/example/unprotected/CADPRO-Sample-Fusion-Addin.c')
    shutil.move("CADPRO-Sample-Fusion-Addin.pyd", './../fusion_addins/example/unprotected')