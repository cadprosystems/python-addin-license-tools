import struct
import json

# Handles reading/writing messages (JSON objects) from/to a TCP socket.
# Messages are encoded as a four-byte header (an integer indicating the payload length) and a payload which is a UTF-8 encoded JSON string.
class Message:
    def __init__(self):
        self.reset()
    
    # Get the data in the Message, as it would appear on the socket.
    def getData(self):
        return self.data
    
    # Append a byte received from the socket.
    def appendByte(self, byte):
        self.data.append(byte[0])
    
    # Get the length of the message payload. Only call this after the 4-byte header is loaded.
    def getPayloadLength(self):
        return struct.unpack('i', self.data[0:4])[0]
    
    # Returns True if the message has been fully loaded.
    # This means the header is loaded, and the payload length indicated in the header has been reached.
    def isComplete(self):
        return len(self.data) >= 4 and len(self.data) >= 4 + self.getPayloadLength()
    
    # Clear the Message object so that it can be used for another message.
    def reset(self):
        self.data = bytearray()
    
    # Set the Message object from a Python object. The Python object will be encoded as JSON.
    # After calling this, the bytes returned by getData() can be sent over the socket.
    def setFromObject(self, obj):
        payload = json.dumps(obj)
        self.reset()
        self.data = bytearray(struct.pack('i', len(payload)))
        for b in payload:
            self.data.append(ord(b))

    # Decodes the Python object represented by the message. This should only be called after
    # isComplete() returns True.
    def getAsObject(self):
        payload = self.data[4:4+self.getPayloadLength()]
        payloadStr = payload.decode('utf-8')
        return json.loads(payloadStr)
