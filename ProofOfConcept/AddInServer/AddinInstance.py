from LicenseHandler import LicenseHandler
from Message import Message
from MessageQueue import MessageQueue
from ObjectTable import ObjectTable

class AddinInstance:
	def __init__(self, addin, sock):
		self.addin = addin
		self.sock = sock
		self.isAddinStopped = False
		self.messageQueue = MessageQueue(self.handleMessage)
		self.messageQueue.start(self.sock)
		self.objectIDs = ObjectTable()

	def runUntilStopped(self):
		self.messageQueue.sendMessage({ 'action': 'setTopLevelCallbacks', 'runCallback': self.unwrap(self.addinCallbackRun), 'stopCallback': self.unwrap(self.addinCallbackStop) })
		self.messageQueue.waitForCondition(lambda: self.isAddinStopped or self.messageQueue.isDisconnected)

	# Handle a message received from the socket.
	def handleMessage(self, obj):
		print("Received message")
		# print(repr(obj))

		if 'action' in obj and obj['action'] == 'callCallback':
			result = self.wrap(obj['callback'])(*map(lambda a: self.wrap(a), obj['args']))
			self.messageQueue.sendMessage({ 'correlationID': obj['correlationID'], 'result': result })
			return True

		elif 'action' in obj and obj['action'] == 'callSubclassMethod':
			target = self.wrap(obj['obj'])
			method = getattr(target, obj['methodName'])
			result = self.wrap(method(*map(lambda a: self.wrap(a), obj['methodArgs'])))
			self.messageQueue.sendMessage({ 'correlationID': obj['correlationID'], 'result': result })
			return True

		return False

	# Proxy a method-call over the socket, and return the result.
	def callProxyMethod(self, moduleName, name, *args):
		return self.messageQueue.sendMessageAndReturnResult({ 'action': 'callProxyMethod', 'moduleName': moduleName, 'methodName': name, 'methodArgs': args })

	def addinCallbackRun(self):
		product_code = self.addin.getProductCode()
		self.license_handler = LicenseHandler(product_code)
		print(self.license_handler.product_code, self.license_handler.license_valid)

		return self.addin.run(self.license_handler)


	def addinCallbackStop(self):
		result = self.addin.stop()
		self.isAddinStopped = True
		return result

	def getADSKClassName(self, obj):
		clazz = obj.__class__
		while 'adsk' not in clazz.__module__ and len(clazz.__bases__) > 0:
			clazz = clazz.__bases__[0]

		if 'adsk.' in clazz.__module__:
			return (clazz.__module__[len('asdk.'):], clazz.__name__)

		return (None, None)

	def unwrap(self, arg):
		if hasattr(arg, '_proxy_data'):
			arg = arg._proxy_data

		elif callable(arg):
			objID = self.objectIDs.getIDForObject(arg)
			arg = { 'type': 'callback', 'proxiedObjectID': objID.hex }

		elif isinstance(arg, object):
			(moduleName, adskClass) = self.getADSKClassName(arg)
			if adskClass is not None:
				objID = self.objectIDs.getIDForObject(arg)
				arg = { 'type': 'subclass', 'moduleName': moduleName, 'className': adskClass, 'proxiedObjectID': objID.hex }

		return arg

	def wrap(self, arg):
		from Server import Server

		if isinstance(arg, dict):
			def createWrapperObject():
				if arg['proxiedObjectType'] in Server.instance.cxxTypeToPythonType:
					pythonType = Server.instance.cxxTypeToPythonType[arg['proxiedObjectType']]
					pythonObj = pythonType.__new__(pythonType)
					pythonObj._proxy_data = arg
					return pythonObj
				else:
					raise "Unknown type " + arg['proxiedObjectType']

			arg = self.objectIDs.getObjectWithID(arg['proxiedObjectID'], createWrapperObject)

		return arg
