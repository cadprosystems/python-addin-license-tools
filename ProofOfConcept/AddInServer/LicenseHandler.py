import sys
import os
import requests
import time
import hashlib
import binascii

# Defines globals
desktop_app_uri = "http://localhost:3002"
cps_key = "2FFB9EFB-22DA-4BF5-A390-6D830483AFA3"

class LicenseHandler:
    # Initialiser
    def __init__(self, product_code):
        self.product_code = product_code
        self.license_valid = False
        self.checkForValidLicense()

    # Gets the hash for the given string
    def getHash(self, string):
        byte_string = string.encode('utf-8')
        hash_algorithm = hashlib.sha256(byte_string).hexdigest()

        return hash_algorithm

    # Validates the content of the checklicense request
    def validateResponse(self, timestamp, response_hash):
        # Sets the default response value
        license_valid = False

        # Creates the check hash
        check_hash = f'{timestamp}:{self.product_code}:{cps_key}'
        hash = self.getHash(check_hash)

        # Checks if the hashes are the same
        print()
        print(f'Check    Hash: {hash.lower()}')
        print(f'Response Hash: {response_hash.lower()}')
        print()
        if (hash.lower() == response_hash.lower()):
            license_valid = True

        return license_valid

    # Calls the Desktop App /checklicense endpoint
    def checkForValidLicense(self):
        timestamp = int(round(time.time() * 1000))
        response = requests.get(f'{desktop_app_uri}/checklicense?timestamp={timestamp}&productcode={self.product_code}')
        self.license_valid = self.validateResponse(timestamp, response.content.decode())

        return self.license_valid