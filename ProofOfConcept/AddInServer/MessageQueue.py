import queue
import threading
import uuid

from Message import Message

class MessageQueue:
    def __init__(self, handleMessageFunc, notifyMessageFunc = None):
        self.handleMessageFunc = handleMessageFunc
        self.notifyMessageFunc = notifyMessageFunc
        self.queue = queue.Queue()
        self.pendingCallbacks = dict()
        self.isDisconnected = False

    def start(self, sock):
        self.sock = sock
        threading.Thread(target=lambda: self._receiveThread(), daemon=True).start()

    # Waits for data to be received from the socket, bundles it into Message objects, and
    # puts it into a queue for processing.
    def _receiveThread(self):
        messageIn = Message()

        while not self.isDisconnected:
            try:
                messageIn.appendByte(self.sock.recv(1))
            except:
                self.isDisconnected = True

            if messageIn.isComplete():
                obj = messageIn.getAsObject()
                messageIn.reset()
                self.queue.put(obj)

                if self.notifyMessageFunc:
                    self.notifyMessageFunc()

    # Keep handling incoming messages while waiting for the given condition to become true.
    def waitForCondition(self, func):
        while not func():
            self.handleMessage(self.queue.get())

    def pumpQueue(self):
        while not self.queue.empty():
            self.handleMessage(self.queue.get())

    def handleMessage(self, obj):
        wasHandled = self.handleMessageFunc(obj)
        if not wasHandled:
            correlationID = obj['correlationID']
            if correlationID in self.pendingCallbacks:
                callback = self.pendingCallbacks[correlationID]
                del self.pendingCallbacks[correlationID]
                callback(obj['result'])

    # Send a Python object over the socket.
    def sendMessage(self, obj):
        if self.sock:
            message = Message()
            message.setFromObject(obj)
            print("Sending " + repr(message.getData()))
            self.sock.sendall(message.getData())

    # Helper function that sends a message and waits for the result.
    # This uses busy-waiting, which is not ideal. It should be refactored.
    def sendMessageAndReturnResult(self, obj):
        # Generate a correlation ID so that we can recognize the response when it comes back.
        correlationID = uuid.uuid4().hex
        obj['correlationID'] = correlationID

        # A place for the callback to store the result when it gets it.
        data = { 'hasResult': False, 'result': None }

        # Set up the callback.
        def callback(r):
            data['hasResult'] = True
            data['result'] = r
        self.pendingCallbacks[correlationID] = callback

        # Fire off the message.
        self.sendMessage(obj)

        self.waitForCondition(lambda: data['hasResult'])

        # Got the result, now we can return it.
        return data['result']
