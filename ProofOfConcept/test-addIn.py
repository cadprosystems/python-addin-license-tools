#Author-
#Description-

import adsk._core, adsk._fusion, adsk._cam

import struct
import json
import uuid

# Handles reading/writing messages (JSON objects) from/to a TCP socket.
# Messages are encoded as a four-byte header (an integer indicating the payload length) and a payload which is a UTF-8 encoded JSON string.
class Message:
    def __init__(self):
        self.reset()
    
    # Get the data in the Message, as it would appear on the socket.
    def getData(self):
        return self.data
    
    # Append a byte received from the socket.
    def appendByte(self, byte):
        self.data.append(byte[0])
    
    # Get the length of the message payload. Only call this after the 4-byte header is loaded.
    def getPayloadLength(self):
        return struct.unpack('i', self.data[0:4])[0]
    
    # Returns True if the message has been fully loaded.
    # This means the header is loaded, and the payload length indicated in the header has been reached.
    def isComplete(self):
        return len(self.data) >= 4 and len(self.data) >= 4 + self.getPayloadLength()
    
    # Clear the Message object so that it can be used for another message.
    def reset(self):
        self.data = bytearray()
    
    # Set the Message object from a Python object. The Python object will be encoded as JSON.
    # After calling this, the bytes returned by getData() can be sent over the socket.
    def setFromObject(self, obj):
        payload = json.dumps(obj)
        self.reset()
        self.data = bytearray(struct.pack('i', len(payload)))
        for b in payload:
            self.data.append(ord(b))

    # Decodes the Python object represented by the message. This should only be called after
    # isComplete() returns True.
    def getAsObject(self):
        payload = self.data[4:4+self.getPayloadLength()]
        payloadStr = payload.decode('utf-8')
        return json.loads(payloadStr)

# Waits for data to be received from the socket, bundles it into Message objects, and
# passes those Message objects to handleMessage().
# NOTE: Not thread-safe!
def receiveThread(s):
    message = Message()
    
    while True:
        message.appendByte(s.recv(1))

        if message.isComplete():
            handleMessage(message.getAsObject())
            message.reset()

# Generates and stores unique IDs for objects, so that they can be referenced in proxy objects.
# NOTE: This does not delete objects when they are no longer used, so it will leak memory over time!
class ObjectTable:
    def __init__(self):
        self.entries = []

    # Gets a unique ID for the given object, generating one if it doesn't already exist.
    def getIDForObject(self, obj):
        for (k, v) in self.entries:
            if k == obj:
                return v
                
        v = uuid.uuid4()
        self.entries.append((obj, v))
        return v
        
    # Gets the object with the given unique ID.
    def getObjectWithID(self, objID):
        for (k, v) in self.entries:
            if v.hex == objID:
                return k
                
        raise 'Object not found!'

objectIDs = ObjectTable()

# Returns True if this object is an Autodesk proxy object.
# If so, we need to wrap it in our own proxy before it is sent over the socket.
# If not, we *should* be fine to just serialize it as JSON.
def isAutodeskProxy(obj):
    # TODO: Find a more efficient way of determining this!
    return "proxy of <Swig Object of type" in repr(obj)

# Unwraps an object received over the socket, returning the original object.
def unwrapObject(obj):
    if isinstance(obj, dict) and obj['type'] == 'proxy' and obj['proxiedObjectID']:
        return objectIDs.getObjectWithID(obj['proxiedObjectID'])
    else:
        return obj

# Wraps an object for sending over the socket.
def wrapObject(obj):
    if isAutodeskProxy(obj):
        objectID = objectIDs.getIDForObject(obj)
        return { 'type': 'proxy', 'proxiedObjectID': objectID.hex }
    else:
        return obj

# Calls an Autodesk API method with the given arguments. Currently we assume it's in adsk._core.
def callProxyMethod(methodName, methodArgs):
    # Get the method.
    method = getattr(adsk._core, methodName)
    print(repr(method))
    
    # Unwrap the arguments.
    unwrappedMethodArgs = list(map(lambda a: unwrapObject(a), methodArgs))
    print(repr(unwrappedMethodArgs))
    
    # Call the method.
    result = method(*unwrappedMethodArgs)
    print(repr(result))
    
    # Wrap the result for sending back over the socket.
    wrappedResult = wrapObject(result)
    print(repr(wrappedResult))
    
    return wrappedResult

# Handle a message received from the socket.
def handleMessage(obj):
    print(repr(obj))
    
    if obj['action'] == 'callProxyMethod':
        # Call the method.
        result = callProxyMethod(obj['methodName'], obj['methodArgs'])
        
        # Send the result back.
        sendMessage({ 'correlationID': obj['correlationID'], 'result': result })

# A bit hacky - this is how sendMessage() gets access to the socket.
theSocket = None

# Send a Python object over the socket.
def sendMessage(obj):
    global theSocket
    s = theSocket  

    message = Message()
    message.setFromObject(obj)
    print("Sending " + repr(message.getData()))
    s.sendall(message.getData())

# Entry point.
def test():
    import socket
    import threading
    global theSocket
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    s.connect(('localhost', 1235))
    theSocket = s
    
    receiveThread(s) # Turns out we can't do this off the UI thread, or our Autodesk API calls will fail.
    #threading.Thread(target=receiveThread, args=(s,)).start()

    return
    
def run(context):
    test()

def stop(context):
    pass
