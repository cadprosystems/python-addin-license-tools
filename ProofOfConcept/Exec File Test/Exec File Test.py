#Author-Nick Hall - CADPRO
#Description-Testing an idea

import adsk.core, adsk.fusion, adsk.cam, traceback
import os
from datetime import datetime

def run(context):
    ui = None
    try:
        exec_code() 

    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))


def exec_code(): 
    app = adsk.core.Application.get()
    ui  = app.userInterface
    LOC = __file__
    ui.messageBox(LOC)
    LOC = os.path.dirname(__file__)
    ui.messageBox(LOC)
    f = open(os.path.dirname(__file__) + "/ExecScript.py", "r")
    LOC = f.read()
    ui.messageBox(LOC)
    exec(LOC) 
      
