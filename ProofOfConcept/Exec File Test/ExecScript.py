#Author-Nick Hall - CADPRO
#Description-Testing an idea

def getmsg():
    return 'Hello test script from ExecScript.Py! At ' + datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)")

app = adsk.core.Application.get()
ui  = app.userInterface
msg = getmsg()
ui.messageBox(msg)
