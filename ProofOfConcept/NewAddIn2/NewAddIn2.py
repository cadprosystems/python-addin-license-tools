#Author-
#Description-

# import adsk.core
import adsk._core, adsk._fusion, adsk._cam

import sys
import os

sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'Shared')))

from Message import Message
from MessageQueue import MessageQueue
from ObjectTable import ObjectTable

threadEvent = None
app = None
isMainThreadBlocked = False
ui = None

# Returns True if this object is an Autodesk proxy object.
# If so, we need to wrap it in our own proxy before it is sent over the socket.
# If not, we *should* be fine to just serialize it as JSON.
def isAutodeskProxy(obj):
	# TODO: Find a more efficient way of determining this!
	import re
	return not hasattr(obj, '_proxy_data') and (re.search('<adsk\\.([a-z]+)\\.([^;]+); proxy of', repr(obj)) or re.search('\'adsk::([a-z]+)::([^ ]+) ', repr(obj)))

def getAutodeskModule(moduleName, isNative):
	import importlib

	if isNative:
		return importlib.import_module('adsk._' + moduleName)
	else:
		return importlib.import_module('adsk.' + moduleName)

# Unwraps an object received over the socket, returning the original object.
def unwrapObject(obj):
	if isinstance(obj, dict) and obj['type'] == 'proxy' and obj['proxiedObjectID']:
		return objectIDs.getObjectWithID(obj['proxiedObjectID'])
	elif isinstance(obj, dict) and obj['type'] == 'subclass' and obj['proxiedObjectID'] and obj['moduleName'] and obj['className']:
		def creator():
			baseClass = getattr(getAutodeskModule(obj['moduleName'], False), obj['className'])
			subclass = makeFakeSubclass(baseClass, lambda methodName, *args: callSubclassMethod(methodName, obj, args))
			wrappedObj = subclass()
			#wrappedObj._proxy_data = obj
			return wrappedObj
		return objectIDs.getObjectWithID(obj['proxiedObjectID'], creator)
	elif isinstance(obj, dict) and obj['type'] == 'callback' and obj['proxiedObjectID']:
		def dummyCallback(*args):
			return messageQueue.sendMessageAndReturnResult({ 'action': 'callCallback', 'callback': obj, 'args': list(map(lambda a: wrapObject(a), args)) })
		return dummyCallback
	else:
		return obj

# Wraps an object for sending over the socket.
def wrapObject(obj):
	if hasattr(obj, '_proxy_data'):
		return obj._proxy_data
	
	elif isAutodeskProxy(obj):
		import re
		objectID = objectIDs.getIDForObject(obj)
		try:              
			proxiedObjectType = re.search('<adsk\\.([a-z]+)\\.([^;]+); proxy of', repr(obj)).expand('adsk::\\1::\\2')
		except:
			proxiedObjectType = re.search('\'adsk::([a-z]+)::([^ ]+) ', repr(obj)).expand('adsk::\\1::\\2')
		return { 'type': 'proxy', 'proxiedObjectID': objectID.hex, 'proxiedObjectType': proxiedObjectType }
	else:
		return obj

def callSubclassMethod(name, obj, args):
	global isMainThreadBlocked
	isMainThreadBlocked = True
	# TODO: We need to wait for the result here, otherwise the caller may delete the arguments before we've finished using them.
	result = messageQueue.sendMessageAndReturnResult({ 'action': 'callSubclassMethod', 'obj': obj, 'methodName': name, 'methodArgs': list(map(lambda a: wrapObject(a), args)) })
	isMainThreadBlocked = False
	return result

# Calls an Autodesk API method with the given arguments.
def callProxyMethod(moduleName, methodName, methodArgs):
	# Get the method.
	method = getattr(getAutodeskModule(moduleName, True), methodName)
	print('METHOD ' + repr(method))
	
	# Unwrap the arguments.
	unwrappedMethodArgs = list(map(lambda a: unwrapObject(a), methodArgs))
	print('ARGS ' + repr(unwrappedMethodArgs))
	
	# Call the method.
	result = method(*unwrappedMethodArgs)
	print('RESULT ' + repr(result))
	
	# Wrap the result for sending back over the socket.
	wrappedResult = wrapObject(result)
	print('WRAPPED RESULT ' + repr(wrappedResult))
	
	return wrappedResult

topLevelCallbacks = None

# Handle a message received from the socket.
def handleMessage(obj):
	global topLevelCallbacks
	print(repr(obj))
	
	if 'action' in obj and obj['action'] == 'callProxyMethod':
		# Call the method.
		result = callProxyMethod(obj['moduleName'], obj['methodName'], obj['methodArgs'])
		
		# Send the result back.
		messageQueue.sendMessage({ 'correlationID': obj['correlationID'], 'result': result })
		return True

	elif 'action' in obj and obj['action'] == 'setTopLevelCallbacks':
		if topLevelCallbacks is None:
			topLevelCallbacks = { 'run': unwrapObject(obj['runCallback']), 'stop': unwrapObject(obj['stopCallback']) }
			topLevelCallbacks['run']()
			return True
		else:
			raise 'Top-level callbacks already set!'

	return False

def notifyMainThreadOfMessage():
	global isMainThreadBlocked
	if not isMainThreadBlocked:
		isMainThreadBlocked = True
		app.fireCustomEvent('massif_thread_event', '')

messageQueue = MessageQueue(handleMessage, notifyMainThreadOfMessage)
objectIDs = ObjectTable()

class ThreadEventHandler(adsk.core.CustomEventHandler):
	def __init__(self):
		super().__init__()

	def notify(self, args):
		try:
			print("GOT EVENT")
			messageQueue.pumpQueue()
			global isMainThreadBlocked
			isMainThreadBlocked = False
		except:
			if ui:
				ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

handlers = []

# Entry point.
def run(context):
	try:
		import socket
		import threading
		global threadEvent
		global handlers
		global app
		global ui

		app = adsk.core.Application.get()
		ui = app.userInterface
		threadEvent = app.registerCustomEvent('massif_thread_event')
		handler = ThreadEventHandler()
		threadEvent.add(handler)
		handlers.append(handler)
		
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		
		s.connect(('localhost', 1235))
		
		messageQueue.start(s)
	except:
		if ui:
			ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))


def stop(context):
	try:
		topLevelCallbacks['stop']()

		global handlers
		for handler in handlers:
			threadEvent.remove(handler)
		app.unregisterCustomEvent('massif_thread_event')
		handlers = []
	except:
		if ui:
			ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

# run(None)

def makeFakeSubclass(base, callMethod):
	class FakeSubclass(base):
		def __init__(self):
			print("FakeModule: before __init__")
			super().__init__()
			print("FakeModule: after __init__")

		def notify(self, *args):
			callMethod('notify', *args)

		#def __getattr__(self, name):
			#if name == 'this':
			#    return self

			#if name == 'append':
			#    return lambda *args: None

			#adsk.core.Application.get().userInterface.messageBox("FakeSubclass.__getattr__(" + name + ")")
		#    return lambda *args: callMethod(name, *args)
			
	return FakeSubclass
