import uuid

# Generates and stores unique IDs for objects, so that they can be referenced in proxy objects.
# NOTE: This does not delete objects when they are no longer used, so it will leak memory over time!
class ObjectTable:
	def __init__(self):
		self.entries = []

	# Gets a unique ID for the given object, generating one if it doesn't already exist.
	def getIDForObject(self, obj):
		for (k, v) in self.entries:
			if k == obj:
				return v
				
		v = uuid.uuid4()
		self.entries.append((obj, v))
		return v
		
	# Gets the object with the given unique ID.
	def getObjectWithID(self, objID, creator=None):
		for (k, v) in self.entries:
			if v.hex == objID:
				return k
				
		if creator is None:
			raise 'Object not found!'
		else:
			newObj = creator()
			self.entries.append((newObj, uuid.UUID(objID)))
			return newObj

	# Adds the given object/ID mapping to the table.
	def addObjectWithID(self, objID, obj):
		for (k, v) in self.entries:
			if k == obj:
				if v.hex == objID:
					return
				else:
					raise 'Object already exists with a different ID.'
		
		self.entries.append((obj, uuid.UUID(objID)))
