import struct
import json
import uuid
import requests
import time
import hashlib
import binascii

desktop_app_uri = "http://localhost:3002"
cps_key = "2FFB9EFB-22DA-4BF5-A390-6D830483AFA3"
product_code = "CPSP-MZKIJS"

# Handles reading/writing messages (JSON objects) from/to a TCP socket.
# Messages are encoded as a four-byte header (an integer indicating the payload length) and a payload which is a UTF-8 encoded JSON string.
class Message:
    def __init__(self):
        self.reset()
    
    # Get the data in the Message, as it would appear on the socket.
    def getData(self):
        return self.data
    
    # Append a byte received from the socket.
    def appendByte(self, byte):
        self.data.append(byte[0])
    
    # Get the length of the message payload. Only call this after the 4-byte header is loaded.
    def getPayloadLength(self):
        return struct.unpack('i', self.data[0:4])[0]
    
    # Returns True if the message has been fully loaded.
    # This means the header is loaded, and the payload length indicated in the header has been reached.
    def isComplete(self):
        return len(self.data) >= 4 and len(self.data) >= 4 + self.getPayloadLength()
    
    # Clear the Message object so that it can be used for another message.
    def reset(self):
        self.data = bytearray()
    
    # Set the Message object from a Python object. The Python object will be encoded as JSON.
    # After calling this, the bytes returned by getData() can be sent over the socket.
    def setFromObject(self, obj):
        payload = json.dumps(obj)
        self.reset()
        self.data = bytearray(struct.pack('i', len(payload)))
        for b in payload:
            self.data.append(ord(b))

    # Decodes the Python object represented by the message. This should only be called after
    # isComplete() returns True.
    def getAsObject(self):
        payload = self.data[4:4+self.getPayloadLength()]
        payloadStr = payload.decode('utf-8')
        return json.loads(payloadStr)

# Waits for data to be received from the socket, bundles it into Message objects, and
# passes those Message objects to handleMessage().
# NOTE: Not thread-safe!
def receiveThread(s):
    message = Message()
    
    while True:
        message.appendByte(s.recv(1))

        if message.isComplete():
            handleMessage(message.getAsObject())
            message.reset()

# Holds callbacks for requests that have been sent over the socket, while we wait for responses to them.
# NOTE: Definitely not thread-safe!
pendingCallbacks = dict()

# Handle a message received from the socket.
# Currently the only messages we receive on the Server are responses to our calls.
def handleMessage(obj):
    # print(repr(obj))
    correlationID = obj['correlationID']
    if correlationID in pendingCallbacks:
        callback = pendingCallbacks[correlationID]
        del pendingCallbacks[correlationID]
        callback(obj['result'])

# A bit hacky - this is how sendMessage() gets access to the socket.
theSocket = None

# Send a Python object over the socket.
def sendMessage(obj):
    global theSocket
    s = theSocket  

    message = Message()
    message.setFromObject(obj)
    # print("Sending " + repr(message.getData()))
    s.sendall(message.getData())

# Helper function that sends a message and waits for the result.
# This uses busy-waiting, which is not ideal. It should be refactored.
def sendMessageAndReturnResult(obj):
    # Generate a correlation ID so that we can recognize the response when it comes back.
    correlationID = uuid.uuid4().hex
    obj['correlationID'] = correlationID

    # A place for the callback to store the result when it gets it.
    data = { 'result': None }

    # Set up the callback.
    def callback(r):
        data['result'] = r
    pendingCallbacks[correlationID] = callback

    # Fire off the message.
    sendMessage(obj)

    # Busy-waiting - yuck!
    while data['result'] is None:
        pass

    # Got the result, now we can return it.
    return data['result']

# Proxy a method-call over the socket, and return the result.
def callProxyMethod(name, *args):
    return sendMessageAndReturnResult({ 'action': 'callProxyMethod', 'methodName': name, 'methodArgs': args })


# Gets the hash for the given string
def getHash(string):
    byte_string = string.encode('utf-8')
    # print(f'Encoded string: {byte_string}')
    hash_algorithm = hashlib.sha256(byte_string).hexdigest()
    # print(f'Hash Hex: {hash_algorithm}')

    return hash_algorithm

# Validates the content of the checklicense request
def validateResponse(timestamp, response_hash):
    # Sets the default response value
    license_valid = False

    # Creates the check hash
    check_hash = f'{timestamp}:{product_code}:{cps_key}'
    # print(f'Check Hash (un-encrypted): {check_hash}')
    hash = getHash(check_hash)
    # print(f'Check Hash (encrypted): {hash}')

    # Checks if the hashes are the same
    print()
    print(f'Check    Hash: {hash.lower()}')
    print(f'Response Hash: {response_hash.lower()}')
    if (hash.lower() == response_hash.lower()):
        license_valid = True

    return license_valid

# Calls the Desktop App /checklicense endpoint
def checkForValidLicense():
    timestamp = int(round(time.time() * 1000))
    # print(f'Timestamp: {timestamp}')
    response = requests.get(f'{desktop_app_uri}/checklicense?timestamp={timestamp}&productcode={product_code}')

    # print(f'License Data')
    # print(f'Status Code: {response.status_code}')
    # print(f'Encoding: {response.encoding}')
    # print(f'Body: {response.content.decode()}')

    return validateResponse(timestamp, response.content.decode())

# Super secret code goes in here.
def runAddIn():
    # Makes request to Desktop App to check the license
    license_available = checkForValidLicense()

    # Creates UI elements for response
    app = callProxyMethod("Application_get")
    ui = callProxyMethod("Application__get_userInterface", app)
    palettes = callProxyMethod('UserInterface_palettes', ui)
    print(f'Palettes: {palettes}')

    


# Server entry point.
def test():
    import socket
    import threading
    global theSocket

    # Creates the Socket and listens on port 1235
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 1235))
    s.listen(0)

    while True:
        try:
            # Waits for a connection
            conn, addr = s.accept()
            print(f'Connected by: {addr}')
            theSocket = conn

            # The receive loop needs to run on a separate thread so that we don't get deadlocked when waiting for a response.
            thread = threading.Thread(target=receiveThread, args=(conn,))
            thread.start()
            
            runAddIn()
            thread.join()

            return
        except Exception as error:
            print(f'Error: {error}')
test()